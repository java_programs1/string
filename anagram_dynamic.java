class Solution
{    
   
    public static boolean isAnagram(String a,String b)
    {
        if(a.length()!=b.length())
        {
            return false;
        }
      
        a=a.toLowerCase();
        b=b.toLowerCase();
        
        char[] arr1=a.toCharArray();
        char[] arr2=b.toCharArray();
        
       Arrays.sort(arr1);
       Arrays.sort(arr2);
        
   for(int i=0;i<a.length();i++)
  {
     if(arr1[i]!=arr2[i])
     {
         return false;
     }
  }
  return true;
     
    }
}
