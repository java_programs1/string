//Find First Palindromic String in the Array

package Array_swap;

public class leetcode_palindrome {
	public static void main(String[] args) {
		String[] words = {"abc","car","ada","racecar","cool"};
		
		System.out.println(firstPalindrome(words));

	}
	private static String firstPalindrome(String[] words) {
		
		for(int i=0;i<words.length;i++)
		{
			String check=find_palindrome(words[i]);
			if(words[i].equals(check))
			{
				return words[i];
			}
		}
		return "";
	}
	private static String find_palindrome(String word) {
		
		String output="";
		for(int i=word.length()-1;i>=0;i--)
		{
			output+=word.charAt(i);
		}		
		return output;
	}	
}

